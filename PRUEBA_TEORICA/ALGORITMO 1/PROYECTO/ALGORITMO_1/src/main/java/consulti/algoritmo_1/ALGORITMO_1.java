/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package consulti.algoritmo_1;

import java.util.Scanner;

/**
 *
 * @author leonel
 */
public class ALGORITMO_1 {

    public static void main(String[] args) {
         // Scanner scanner = new Scanner(System.in);
        //Aarray
        int[] numeros = {1, 2, 2, 5, 4, 6, 7, 8, 8, 8};
        int contador = 0;
        int auxCount = 1;
        
        int maxNumeroRepetido =  numeros[0];//Obtener el primer valor del array
        
        for (int i = 1; i < numeros.length; i++) {
            System.out.println(numeros[i]+ "="+ numeros[i - 1]);
            if (numeros[i] == numeros[i - 1]) {
                auxCount++; //2
            } else {
                if (auxCount > contador) {
                    contador = auxCount;
                    maxNumeroRepetido = numeros[i - 1];
                }
                auxCount = 1;
            }
        }
        
        if (auxCount > contador) {
            contador = auxCount;
            maxNumeroRepetido = numeros[numeros.length - 1];
        }
        
        System.out.println("Recurrencias: " + contador);
        System.out.println("Número: " + maxNumeroRepetido);
    }
    
    
    
    
    
    
    
   
}
