/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package consulti.algoritmo_2;

/**
 *
 * @author leonel
 */
public class ALGORITMO_2 {

    public static void main(String[] args) {
       
      
        char[][] area = new char[4][4];
        int[] myArray = {1, 2, -1, 1, 0, 1, 2, -1, -1, -2};

        // Se declara una matriz 4x4
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                area[i][j] = 'O';
            }
        }

        int x = 0; // Posición inicial en el eje horizontal
        int y = 0; // Posición inicial en el eje vertical

        for (int i = 0; i < myArray.length; i += 2) {
            int dx = myArray[i];     // Movimiento en el eje horizontal
            int dy = myArray[i + 1]; // Movimiento en el eje vertical

            // Actualizar la posición de la X
            x += dx;
            y += dy;

            // Limitar la posición de la X al área de 4x4
            //Devuelve el valor mas alto
            x = Math.max(0, Math.min(3, x));
            y = Math.max(0, Math.min(3, y));
            area[y][x] = 'X';  // Marcar la posición actual de la X en el área
        }

        // Imprimir el área final con la posición de la X
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                System.out.print(area[i][j]);
            }
            System.out.println();
        }
    }
}
